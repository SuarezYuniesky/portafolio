package firebase.app.calculator

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import java.lang.ArithmeticException

class MainActivity : AppCompatActivity() {

    //me dice si el ultimo boton que presione fue
    // un numerico,inicializdo en falso
    var lastNumeric : Boolean = false
    //me dice si el ultimo boton que presione fue
    // un punto,inicializdo en falso
    var lastDot : Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun onDigit(view:View){
        tvInput.append((view as Button).text) //obtener el texto de cada boton y mostrarlo
        lastNumeric = true  //resetea a true la intruccion de fun onDecimalPoint(view: View) cada vez que se marca un numerico

    }

    fun onClear (view: View){
        tvInput.text = "" //limpia la caja de tvInput
        //con estas dos sentencias las ponemos en poscion iniciar despues de limpiar la caja
        lastNumeric = false
        lastDot = false

    }

    //funcion para un solo punto desimal
    fun onDecimalPoint(view: View){
        //chequemamo que el ultimo boton fue un numerico para poder representar el punto
        if (lastNumeric && !lastDot){
            tvInput.append(".")    //marcamos el punto
            lastNumeric = false
            lastDot = true       //reseteamos a true para mostarr que ya fue usado
        }
    }


    fun onOperator(view: View){
      if (lastNumeric && ! isOperatoradded(tvInput.text.toString())){
          tvInput.append((view as Button).text)
          lastNumeric = false
          lastDot = false
      }
    }

    //chequear si se adicionaron los operadores y regresa verdadero o falso
    private fun isOperatoradded(value : String) : Boolean{ //el numero se guardar en value en forma de string
        return  if (value.startsWith("-")){  //preguntamos si el numero comienza con un (-)
            false        //falso si tiene un menos delante
        }else{ //chequeamos si valor contiene algunos de estos operadores
            value.contains("/") || value.contains("+") || value.contains("-")
                    || value.contains("*")
        }


    }

    private fun removeCeroAfterDot(result : String) :String{
    var value = result
        if (result.contains(".0"))
        value = result.substring(0,result.length - 2)
        return value
    }

    fun onEqual(view: View){
        if (lastNumeric){
            var tvValue = tvInput.text.toString()
            var prefix = ""
            try{
                if (tvValue.startsWith("-")){
                    prefix = "-"
                    tvValue = tvValue.substring(1)
                }

                if (tvValue.contains("-")){
                     val splitValue = tvValue.split("-")
                     var one = splitValue[0]
                     var two = splitValue[1]

                    if (!prefix.isEmpty()){
                        one = prefix + one
                    }

                    tvInput.text = removeCeroAfterDot( (one.toDouble() - two.toDouble()).toString())

                }    else  if (tvValue.contains("+")){
                    val splitValue = tvValue.split("+")
                    var one = splitValue[0]
                    var two = splitValue[1]

                    if (!prefix.isEmpty()){
                        one = prefix + one
                    }
                    tvInput.text = removeCeroAfterDot( (one.toDouble() + two.toDouble()).toString())

                }   else  if (tvValue.contains("*")){
                    val splitValue = tvValue.split("*")
                    var one = splitValue[0]
                    var two = splitValue[1]

                    if (!prefix.isEmpty()){
                        one = prefix + one
                    }
                    tvInput.text = removeCeroAfterDot( (one.toDouble() * two.toDouble()).toString())

                }   else  if (tvValue.contains("/")){
                    val splitValue = tvValue.split("/")
                    var one = splitValue[0]
                    var two = splitValue[1]

                    if (!prefix.isEmpty()){
                        one = prefix + one
                    }
                    tvInput.text = removeCeroAfterDot( (one.toDouble() /two.toDouble()).toString())
                }
            }catch (e : ArithmeticException){
                e.printStackTrace()
            }
        }
    }
}

