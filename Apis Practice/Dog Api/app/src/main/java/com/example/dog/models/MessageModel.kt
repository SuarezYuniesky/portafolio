package com.example.dog.models

data class MessageModel(
    val message : ArrayList<String>,
    val status : String
)