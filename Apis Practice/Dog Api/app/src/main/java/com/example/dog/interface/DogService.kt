package com.example.dog.`interface`

import com.example.dog.models.MessageModel
import retrofit.Call
import retrofit.http.GET
import retrofit.http.Path

interface DogService {
   @GET("hound/images")
   fun getImages() : Call<MessageModel> // get the whole list

   @GET("{name}/images")
   fun getByBreed(
      @Path("name") name : String
   ) : Call<MessageModel> // get by breed
}