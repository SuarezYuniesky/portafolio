package com.example.dog.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.dog.R
import kotlinx.android.synthetic.main.dog_item.view.*

open class DogAdapter(private val context: Context, private val postList : ArrayList<String>)
    : RecyclerView.Adapter<RecyclerView.ViewHolder>(){

    class MyViewHolder(view : View) : RecyclerView.ViewHolder(view)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return MyViewHolder(
            LayoutInflater.from(context)
            .inflate(R.layout.dog_item,parent,false))
    }

    override fun getItemCount(): Int {
        return postList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val model = postList[position]

        if (holder is MyViewHolder){

            Glide
                .with(context)
                .load(model)
                .centerCrop()
                .placeholder(R.drawable.ic_android_black_24dp)
                .into(holder.itemView.image_holder);



        }


    }
}
