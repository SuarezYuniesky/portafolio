package com.example.dog

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.dog.`interface`.DogService
import com.example.dog.adapters.DogAdapter
import com.example.dog.models.MessageModel
import kotlinx.android.synthetic.main.activity_main.*
import retrofit.GsonConverterFactory
import retrofit.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val retrofit = getRetrofit("https://dog.ceo/api/breed/")

        val service : DogService =
                retrofit.create(DogService::class.java)

        val call = service.getImages()

        call.enqueue(object : Callback<MessageModel>{
            override fun onFailure(t: Throwable?) {
                TODO("Not yet implemented")
            }
            override fun onResponse(response: Response<MessageModel>?, retrofit: Retrofit?) {
                if (response!!.isSuccess){
                    val message : MessageModel = response.body()
                    Log.i("Response","$message")
                    rv_images.layoutManager = LinearLayoutManager(this@MainActivity)
                    rv_images.setHasFixedSize(true)
                    val adapter = DogAdapter(this@MainActivity,message.message as ArrayList<String>)
                    rv_images.adapter = adapter
                }

            }

        })

        btn_find.setOnClickListener {
            val name = et_find_breed.text.toString().toLowerCase()
            val retrofit = getRetrofit("https://dog.ceo/api/breed/")

            val service : DogService =
                retrofit.create(DogService::class.java)

            val call = service.getByBreed(name)

            call.enqueue(object : Callback<MessageModel>{
                override fun onFailure(t: Throwable?) {
                    TODO("Not yet implemented")
                }
                override fun onResponse(response: Response<MessageModel>?, retrofit: Retrofit?) {
                    if (response!!.isSuccess){
                        val message : MessageModel = response.body()
                        Log.i("Response","$message")
                        rv_images.layoutManager = LinearLayoutManager(this@MainActivity)
                        rv_images.setHasFixedSize(true)
                        val adapter = DogAdapter(this@MainActivity,message.message as ArrayList<String>)
                        rv_images.adapter = adapter
                    }

                }

            })
        }
    }

    private fun getRetrofit(baseUrl : String) : Retrofit {
        return Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
    }
}