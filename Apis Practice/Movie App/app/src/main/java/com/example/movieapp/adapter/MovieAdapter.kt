package com.example.movieapp.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.movieapp.R
import com.example.movieapp.models.movie.popularResponse.Result
import kotlinx.android.synthetic.main.item_movie.view.*
import java.util.zip.Inflater

class MovieAdapter (private val context : Context,private val movieList : ArrayList<Result>)
    : RecyclerView.Adapter<RecyclerView.ViewHolder>(){

    class MyViewHolder(view : View) : RecyclerView.ViewHolder(view)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
      return MyViewHolder(LayoutInflater.from(context)
          .inflate(R.layout.item_movie,parent,false))
    }

    override fun getItemCount(): Int {
      return movieList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val initialLink = "https://image.tmdb.org/t/p/original"
       val model = movieList[position]
        val completeLink = initialLink + model.poster_path
        if (holder is MyViewHolder){
            Glide
                .with(context)
                .load(completeLink)
                .centerCrop()
                .placeholder(R.drawable.ic_image_place)
                .into(holder.itemView.iv_holder);
        }
    }
}