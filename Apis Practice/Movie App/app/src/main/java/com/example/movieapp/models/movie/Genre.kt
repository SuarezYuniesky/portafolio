package com.example.movieapp.models.movie

data class Genre(
    val id: Int,
    val name: String
)