package com.example.movieapp.`interface`


import com.example.movieapp.models.movie.popularResponse.PopularResponse
import retrofit.Call
import retrofit.http.GET
import retrofit.http.Query

interface MovieService {
@GET("movie/popular")
 fun getPopularMovies(
    @Query("api_key") api_key : String
) : Call<PopularResponse>

    @GET("trending/movie/week")
    fun getTrendingMovie(
        @Query("api_key") api_key : String
    )  : Call<PopularResponse>
}