package com.example.movieapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.movieapp.`interface`.MovieService
import com.example.movieapp.adapter.MainAdapter
import com.example.movieapp.adapter.MovieAdapter
import com.example.movieapp.models.movie.GeneralModel
import com.example.movieapp.models.movie.MovieResponse
import com.example.movieapp.models.movie.popularResponse.PopularResponse
import com.example.movieapp.models.movie.popularResponse.Result
import com.example.movieapp.utils.Constants
import kotlinx.android.synthetic.main.activity_main.*
import retrofit.*

class MainActivity : AppCompatActivity() {

    private lateinit var popularMovies : ArrayList<Result>
    private lateinit var trendingMovies : ArrayList<Result>

    private lateinit var mainList : ArrayList<GeneralModel>
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        //getPopularMovies()
       // getTrendingMovies()
        mainList.add(GeneralModel("Popular",popularMovies))
        mainList.add(GeneralModel("Trending Movies",trendingMovies))

        //rv_main.layoutManager = LinearLayoutManager(this)
       // rv_main.setHasFixedSize(true)
        //val adapter = MainAdapter(this,mainList)
        //rv_main.adapter = adapter
    }

    private fun getRetrofit() : Retrofit{
        return Retrofit.Builder()
            .baseUrl(Constants.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }



    private fun getPopularMovies(){
        val retrofit = getRetrofit()
        var service : MovieService  = retrofit.create(MovieService::class.java)

        val listCall = service.getPopularMovies(Constants.API_KEY)
        listCall.enqueue(object : Callback<PopularResponse>{
            override fun onFailure(t: Throwable?) {
                Log.i("Response","${t.toString()}")
            }

            override fun onResponse(response: Response<PopularResponse>?, retrofit: Retrofit?) {
               if (response!!.isSuccess){
                   val popular : PopularResponse = response.body()
                   popularMovies = popular.results as ArrayList<Result>
                   /*
                   rv_popular_movie.layoutManager = LinearLayoutManager(this@MainActivity,LinearLayoutManager.HORIZONTAL,false)
                   rv_popular_movie.setHasFixedSize(true)
                   val adapter = MovieAdapter(this@MainActivity,popular.results as ArrayList<Result>)
                   rv_popular_movie.adapter = adapter
                    */
               }
            }

        })
    }


    private fun getTrendingMovies(){
        val retrofit = getRetrofit()
        var service : MovieService  = retrofit.create(MovieService::class.java)

        val listCall = service.getTrendingMovie(Constants.API_KEY)
        listCall.enqueue(object : Callback<PopularResponse>{
            override fun onFailure(t: Throwable?) {
                Log.i("Response","${t.toString()}")
            }

            override fun onResponse(response: Response<PopularResponse>?, retrofit: Retrofit?) {
                if (response!!.isSuccess){
                    val trending : PopularResponse = response.body()
                    trendingMovies = trending.results as ArrayList<Result>
                    /*
                    rv_popular_movie.layoutManager = LinearLayoutManager(this@MainActivity,LinearLayoutManager.HORIZONTAL,false)
                    rv_popular_movie.setHasFixedSize(true)
                    val adapter = MovieAdapter(this@MainActivity,popular.results as ArrayList<Result>)
                    rv_popular_movie.adapter = adapter
                     */
                }
            }

        })
    }
}