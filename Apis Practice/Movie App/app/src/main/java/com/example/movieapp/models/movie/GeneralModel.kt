package com.example.movieapp.models.movie

import com.example.movieapp.models.movie.popularResponse.Result

data class GeneralModel(
    val listName : String,
    val itemList : ArrayList<Result>
)