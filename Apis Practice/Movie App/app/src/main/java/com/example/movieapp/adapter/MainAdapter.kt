package com.example.movieapp.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.movieapp.R
import com.example.movieapp.models.movie.GeneralModel
import kotlinx.android.synthetic.main.activity_main.view.*
import kotlinx.android.synthetic.main.item_main_rv.view.*

class MainAdapter(private val context: Context,private val generalList : ArrayList<GeneralModel>)
    : RecyclerView.Adapter<RecyclerView.ViewHolder>(){

    class MyViewHolder(view : View) : RecyclerView.ViewHolder(view)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return MyViewHolder(LayoutInflater.from(context).
        inflate(R.layout.item_main_rv,parent,false))
    }

    override fun getItemCount(): Int {
       return generalList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
      val model = generalList[position]

        if ( holder is MyViewHolder){
            holder.itemView.tv_list_name.text = model.listName
            holder.itemView.rv_main_movie.layoutManager = LinearLayoutManager(context,LinearLayoutManager.HORIZONTAL,false)
            holder.itemView.rv_main_movie.setHasFixedSize(true)
            val adapter = MovieAdapter(context,model.itemList)
            holder.itemView.rv_main_movie.adapter = adapter
        }

    }
}