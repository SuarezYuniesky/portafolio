package com.example.plants.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.plants.R
import com.example.plants.models.Data
import kotlinx.android.synthetic.main.plant_item.view.*

open class PlantsAdapter(private val context: Context, private val postList : ArrayList<Data>)
    : RecyclerView.Adapter<RecyclerView.ViewHolder>(){

    class MyViewHolder(view : View) : RecyclerView.ViewHolder(view)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return MyViewHolder(
            LayoutInflater.from(context)
                .inflate(R.layout.plant_item,parent,false))
    }
    override fun getItemCount(): Int {
        return postList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val model = postList[position]

        if (holder is MyViewHolder){

            Glide
                .with(context)
                .load(model.image_url)
                .centerCrop()
                .placeholder(R.drawable.ic_android_black_24dp)
                .into(holder.itemView.image_holder);



        }
    }

}