package com.example.plants.models

data class Links(
    val genus: String,
    val self: String,
    val species: String
)