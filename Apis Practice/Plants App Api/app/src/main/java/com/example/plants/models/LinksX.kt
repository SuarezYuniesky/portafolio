package com.example.plants.models

data class LinksX(
    val first: String,
    val last: String,
    val self: String
)