package com.example.plants.`interface`

import com.example.plants.models.DataResponse
import retrofit.Call
import retrofit.http.GET
import retrofit.http.Path
import retrofit.http.Query

interface DataService {
 @GET("plants/search")  //searching by name
 fun getPlantsByCommonName(
     @Query("token") token : String,
     @Query("q") q : String
 ) : Call<DataResponse>

    @GET("plants") //filtering  by  single value in this case name
    fun getPlants(      //it can be also with several values
        @Query("token") token : String,
        @Query("filter[common_name]") name : String
    ) : Call<DataResponse>


}