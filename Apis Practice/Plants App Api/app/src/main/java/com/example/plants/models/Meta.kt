package com.example.plants.models

data class Meta(
    val total: Int
)