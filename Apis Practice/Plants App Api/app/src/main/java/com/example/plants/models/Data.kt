package com.example.plants.models

data class Data(
    val author: String,
    val bibliography: String,
    val common_name: String,
    val complete_data: Boolean,
    val family_common_name: String,
    val genus_id: Int,
    val id: Int,
    val image_url: Any,
    val links: Links,
    val main_species_id: Int,
    val observations: String,
    val scientific_name: String,
    val slug: String,
    val vegetable: Boolean,
    val year: Int
)