package com.example.plants.models

data class DataResponse(
    val `data`: List<Data>,
    val links: LinksX,
    val meta: Meta
)