package com.example.plants

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.plants.`interface`.DataService
import com.example.plants.adapter.PlantsAdapter
import com.example.plants.models.Data
import com.example.plants.models.DataResponse
import kotlinx.android.synthetic.main.activity_main.*
import retrofit.GsonConverterFactory
import retrofit.*

class MainActivity : AppCompatActivity() {
    // pDpBHQeywFPCyNSGEyicKdppJipiWyZmO_KFCbSkbXs
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

    btn_find.setOnClickListener {
        val value = et_find_breed.text.toString().toLowerCase()

        val retrofit = getRetrofit()

        val service : DataService =
            retrofit.create(DataService::class.java)

        val call = service.getPlantsByCommonName("pDpBHQeywFPCyNSGEyicKdppJipiWyZmO_KFCbSkbXs",value)

        call.enqueue(object : Callback<DataResponse>{
            override fun onFailure(t: Throwable?) {
                TODO("Not yet implemented")
            }

            override fun onResponse(response: Response<DataResponse>?, retrofit: Retrofit?) {
                if (response!!.isSuccess){
                    val data : DataResponse = response.body()
                    Toast.makeText(this@MainActivity,data.data[0].image_url.toString(),Toast.LENGTH_LONG).show()
                    rv_images.layoutManager = LinearLayoutManager(this@MainActivity)
                    rv_images.setHasFixedSize(true)
                    val adapter = PlantsAdapter(this@MainActivity,data.data as ArrayList<Data>)
                    rv_images.adapter = adapter
                    Log.i("Response","$data")
                }
            }

        })
    }

    }

    private fun getRetrofit() : Retrofit {
        return Retrofit.Builder()
            .baseUrl("https://trefle.io/api/v1/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }
}