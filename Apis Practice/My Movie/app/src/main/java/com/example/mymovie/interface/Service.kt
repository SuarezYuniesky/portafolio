package com.example.mymovie.`interface`


import com.example.mymovie.models.Response
import com.example.mymovie.models.TrendingResponse
import com.example.mymovie.tvmodels.PopularTVResponse
import com.example.mymovie.tvmodels.TrendingTV
import retrofit.*
import retrofit.http.GET
import retrofit.http.Path
import retrofit.http.Query

interface Service {
    @GET("movie/popular")
    fun getPopularMovies(
        @Query("api_key") api_key : String,
        @Query("page") page : Int
    ) : Call<Response>

    @GET("trending/{media_type}/{time_window}")
    fun getTrendingMovies(
        @Path("media_type") type : String,
        @Path("time_window") time : String,
        @Query("api_key") key : String
    ) : Call<TrendingResponse>

    @GET("tv/popular")
    fun getPopularTv(
        @Query("api_key") api_key : String
    ) : Call<PopularTVResponse>

    @GET("trending/{media_type}/{time_window}")
    fun getTrendingTV(
        @Path("media_type") type : String,
        @Path("time_window") time : String,
        @Query("api_key") key : String
    ) : Call<TrendingTV>
}