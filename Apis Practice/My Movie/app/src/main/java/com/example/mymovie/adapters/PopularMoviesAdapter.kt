package com.example.mymovie.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.mymovie.MainActivity
import com.example.mymovie.R
import com.example.mymovie.models.Result
import kotlinx.android.synthetic.main.simple_item.view.*
import kotlin.properties.Delegates

class PopularMoviesAdapter (private val context: Context,private val pMoviesList : ArrayList<Result>)
    : RecyclerView.Adapter<RecyclerView.ViewHolder>(){
    private var typeMovie : Int = 0
    private var typeAdd : Int = 1

    class MyViewHolder(view : View) : RecyclerView.ViewHolder(view)
    class MyViewHolder2(view : View) : RecyclerView.ViewHolder(view)
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

       return MyViewHolder(LayoutInflater.from(context).inflate(R.layout.simple_item,parent,false))

    }

    override fun getItemCount(): Int {
        return pMoviesList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
       val model = pMoviesList[position]
        val initialLink = "https://image.tmdb.org/t/p/original"

        if (holder is MyViewHolder){
            val completeLink = initialLink + model.poster_path
            if (holder is MyViewHolder){
                Glide
                    .with(context)
                    .load(completeLink)
                    .centerCrop()
                    .placeholder(R.drawable.ic_image_place)
                    .into(holder.itemView.iv_holder);
            }
        }
    }


}