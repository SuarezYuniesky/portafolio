package com.example.mymovie.tvmodels

import com.google.gson.annotations.SerializedName

data class PopularTVResponse(
    val page: Int,
    @SerializedName("results")
    val tvList: List<SerialTv>,
    val total_pages: Int,
    val total_results: Int
)