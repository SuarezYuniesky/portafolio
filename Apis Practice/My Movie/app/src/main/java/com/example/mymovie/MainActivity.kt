package com.example.mymovie

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.core.view.get
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.mymovie.`interface`.Service
import com.example.mymovie.adapters.PopularMoviesAdapter
import com.example.mymovie.adapters.PopularTVAdapter
import com.example.mymovie.adapters.TrendingMoviesAdapter
import com.example.mymovie.adapters.TrendingTVAdapter
import com.example.mymovie.models.Response
import com.example.mymovie.models.Result
import com.example.mymovie.models.ResultX
import com.example.mymovie.models.TrendingResponse
import com.example.mymovie.tvmodels.PopularTVResponse
import com.example.mymovie.tvmodels.SerialTv
import com.example.mymovie.tvmodels.TrendingResult
import com.example.mymovie.tvmodels.TrendingTV
import com.example.mymovie.utils.Constants
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.simple_item.view.*
import retrofit.GsonConverterFactory
import retrofit.*

class MainActivity : AppCompatActivity() {

    var page : Int = 1
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        getPopularMovies()
        getTrendingMovies()
        getPopularSerialTV()
        getTrendingSerialTV()
    }

    private fun getRetrofit() : Retrofit {
        return Retrofit.Builder()
            .baseUrl(Constants.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    

       fun getPopularMovies(){
        val retrofit = getRetrofit()
        var service : Service = retrofit.create(Service::class.java)
        val call = service.getPopularMovies(Constants.API_KEY,page)
        call.enqueue(object : Callback<Response>{
            override fun onFailure(t: Throwable?) {
                TODO("Not yet implemented")
            }

            override fun onResponse(response: retrofit.Response<Response>?, retrofit: Retrofit?) {
                if (response!!.isSuccess){
                    val movies : Response = response.body()
                    rv_p_movies.layoutManager = LinearLayoutManager(
                        this@MainActivity,LinearLayoutManager.HORIZONTAL,false)
                    rv_p_movies.setHasFixedSize(true)
                    val adapter = PopularMoviesAdapter(this@MainActivity,movies.results as ArrayList<Result>)
                    rv_p_movies.adapter = adapter
                }
            }
        })
    }
     private fun getTrendingMovies(){
         val retrofit = getRetrofit()
         var service : Service = retrofit.create(Service::class.java)
         val call = service.getTrendingMovies("movie","week",Constants.API_KEY)
         call.enqueue(object : Callback<TrendingResponse>{
             override fun onFailure(t: Throwable?) {
                 TODO("Not yet implemented")
             }

             override fun onResponse(response: retrofit.Response<TrendingResponse>?, retrofit: Retrofit?) {
                 if (response!!.isSuccess){
                     val movies : TrendingResponse = response.body()
                     rv_t_movies.layoutManager = LinearLayoutManager(
                         this@MainActivity,LinearLayoutManager.HORIZONTAL,false)
                     rv_t_movies.setHasFixedSize(true)
                     val adapter = TrendingMoviesAdapter(this@MainActivity,movies.results as ArrayList<ResultX>)
                     rv_t_movies.adapter = adapter
                 }
             }

         })
     }
   private fun getPopularSerialTV(){
       val retrofit = getRetrofit()
       var service : Service = retrofit.create(Service::class.java)
       val call = service.getPopularTv(Constants.API_KEY)
       call.enqueue(object : Callback<PopularTVResponse>{
           override fun onFailure(t: Throwable?) {
               TODO("Not yet implemented")
           }

           override fun onResponse(
               response: retrofit.Response<PopularTVResponse>?,
               retrofit: Retrofit?
           ) {
              if (response!!.isSuccess){
                  val tv : PopularTVResponse = response.body()
                  rv_tv_popularTV.layoutManager = LinearLayoutManager(
                      this@MainActivity,LinearLayoutManager.HORIZONTAL,false)
                  rv_tv_popularTV.setHasFixedSize(true)
                  val adapter = PopularTVAdapter(this@MainActivity,tv.tvList as ArrayList<SerialTv>)
                  rv_tv_popularTV.adapter = adapter
              }
           }

       })
   }
    private fun getTrendingSerialTV(){
        val retrofit = getRetrofit()
        var service : Service = retrofit.create(Service::class.java)
        val call = service.getTrendingTV("tv","week",Constants.API_KEY)
        call.enqueue(object : Callback<TrendingTV>{
            override fun onFailure(t: Throwable?) {
                TODO("Not yet implemented")
            }

            override fun onResponse(
                response: retrofit.Response<TrendingTV>?,
                retrofit: Retrofit?
            ) {
                if (response!!.isSuccess){
                    val tv : TrendingTV = response.body()
                    rv_tv_trendingTV.layoutManager = LinearLayoutManager(
                        this@MainActivity,LinearLayoutManager.HORIZONTAL,false)
                    rv_tv_trendingTV.setHasFixedSize(true)
                    val adapter = TrendingTVAdapter(this@MainActivity,tv.results as ArrayList<TrendingResult>)
                   rv_tv_trendingTV.adapter = adapter
                }
            }

        })
    }
}