package com.example.mymovie.tvmodels

data class TrendingTV(
    val page: Int,
    val results: List<TrendingResult>,
    val total_pages: Int,
    val total_results: Int
)