package com.example.mymovie.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.mymovie.R
import com.example.mymovie.models.Result
import com.example.mymovie.models.ResultX
import kotlinx.android.synthetic.main.simple_item.view.*

class TrendingMoviesAdapter (private val context: Context, private val tMoviesList : ArrayList<ResultX>)
    : RecyclerView.Adapter<RecyclerView.ViewHolder>(){
    class MyViewHolder(view : View) : RecyclerView.ViewHolder(view)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
      return MyViewHolder(LayoutInflater.from(context).inflate(R.layout.simple_item,parent,false))
    }

    override fun getItemCount(): Int {
        return tMoviesList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
       val model = tMoviesList[position]
        val initialLink = "https://image.tmdb.org/t/p/original"
        if (holder is MyViewHolder){
            val completeLink = initialLink + model.poster_path
            if (holder is MyViewHolder){
                Glide
                    .with(context)
                    .load(completeLink)
                    .centerCrop()
                    .placeholder(R.drawable.ic_image_place)
                    .into(holder.itemView.iv_holder);
            }
        }
    }
}