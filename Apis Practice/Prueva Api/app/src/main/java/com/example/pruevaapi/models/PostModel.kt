package com.example.pruevaapi.models

data class PostModel(
    val userId : Int,
    val id : Int,
    val title : String,
    val body : String
)