package com.example.pruevaapi.models

data class CommentsModel(
    val userId : Int,
    val id : Int,
    val name : String,
    val email : String,
    val body : String
)