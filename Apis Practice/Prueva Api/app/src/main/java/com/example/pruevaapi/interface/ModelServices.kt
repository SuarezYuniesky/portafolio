package com.example.pruevaapi.`interface`

import com.example.pruevaapi.models.PostModel
import retrofit.Call
import retrofit.http.GET
import retrofit.http.Path
import retrofit.http.Query

interface ModelServices {
    //method get for the whole post list
    @GET("posts")
    fun getPostList() : Call<List<PostModel>>

    //method with path parameter to get an object by a given value
    @GET("posts/{id}")
    fun getSpecificPost(@Path("id") id : Int) : Call<PostModel>

    //method to get objects given  certain parameters
    @GET("posts")
    fun getAnotherPostList(
        @Query("userId") userId : Int
    ) : Call<List<PostModel>>
}