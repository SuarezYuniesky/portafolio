package com.example.pruevaapi

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.pruevaapi.`interface`.ModelServices
import com.example.pruevaapi.adapters.PostAdapter
import com.example.pruevaapi.models.PostModel
import com.example.pruevaapi.utils.Constants
import kotlinx.android.synthetic.main.activity_main.*
import retrofit.*

class MainActivity : AppCompatActivity() {
private  var operator: Int = -1
    private var value : Int = 0
    private var value2 : Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

   btn_whole_list.setOnClickListener {
       val retrofit = getRetrofit(Constants.BASE_URL)

       val service : ModelServices =
           retrofit.create(ModelServices::class.java)

       val listCall = service.getPostList()

       listCall.enqueue(object : Callback<List<PostModel>>{
           override fun onFailure(t: Throwable?) {
               TODO("Not yet implemented")
           }
           override fun onResponse(response: Response<List<PostModel>>?, retrofit: Retrofit?) {
               if (response!!.isSuccess){
                   val postList : List<PostModel> = response.body()
                   rv_objects.layoutManager = LinearLayoutManager(this@MainActivity)
                   rv_objects.setHasFixedSize(true)
                   val adapter = PostAdapter(this@MainActivity,postList as ArrayList<PostModel>)
                   rv_objects.adapter = adapter
               }

           }

       })

   }



        btn_specific_list.setOnClickListener {
            val value =  et_specific_li.text.toString().toInt()

            val retrofit = getRetrofit(Constants.BASE_URL)

            val service : ModelServices =
                retrofit.create(ModelServices::class.java)

            val listCall = service.getAnotherPostList(value)

            listCall.enqueue(object : Callback<List<PostModel>>{
                override fun onFailure(t: Throwable?) {
                    TODO("Not yet implemented")
                }
                override fun onResponse(response: Response<List<PostModel>>?, retrofit: Retrofit?) {
                    if (response!!.isSuccess){
                        val postList : List<PostModel> = response.body()
                        rv_objects.layoutManager = LinearLayoutManager(this@MainActivity)
                        rv_objects.setHasFixedSize(true)
                        val adapter = PostAdapter(this@MainActivity,postList as ArrayList<PostModel>)
                        rv_objects.adapter = adapter
                    }

                }

            })

        }
        btn_specific_ob.setOnClickListener {

            val value = et_specific_ob.text.toString().toInt()

            val retrofit = getRetrofit(Constants.BASE_URL)

            val service : ModelServices =
                retrofit.create(ModelServices::class.java)

            val call = service.getSpecificPost(value)

            call.enqueue(object : Callback<PostModel>{
                override fun onFailure(t: Throwable?) {
                    TODO("Not yet implemented")
                }
                override fun onResponse(response: Response<PostModel>?, retrofit: Retrofit?) {
                    if (response!!.isSuccess){
                        val post : PostModel = response.body()
                        Toast.makeText(this@MainActivity,post.id.toString(),Toast.LENGTH_LONG).show()
                        Toast.makeText(this@MainActivity,post.userId.toString(),Toast.LENGTH_LONG).show()
                        Toast.makeText(this@MainActivity,post.body,Toast.LENGTH_LONG).show()
                        Toast.makeText(this@MainActivity,post.title,Toast.LENGTH_LONG).show()
                    }

                }

            })
        }
    }

    private fun getRetrofit(baseUrl : String) : Retrofit{
        return Retrofit.Builder()
            .baseUrl(baseUrl)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }


}