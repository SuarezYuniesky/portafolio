package com.example.pruevaapi.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.pruevaapi.R
import com.example.pruevaapi.models.PostModel
import kotlinx.android.synthetic.main.post_item.view.*

open class PostAdapter(private val context: Context, private val postList : ArrayList<PostModel>)
    : RecyclerView.Adapter<RecyclerView.ViewHolder>(){

    class MyViewHolder(view : View) : RecyclerView.ViewHolder(view)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return MyViewHolder(LayoutInflater.from(context)
            .inflate(R.layout.post_item,parent,false))
    }

    override fun getItemCount(): Int {
        return postList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val model = postList[position]

        if (holder is MyViewHolder){
            holder.itemView.tv_post_id.text = model.userId.toString()
            holder.itemView.tv_id.text = model.id.toString()
            holder.itemView.tv_post_body.text = model.body
            holder.itemView.tv_post_title.text = model.title

        }
    }
}