package com.example.apipost1.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.apipost1.R
import com.example.apipost1.model.Post
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.item_post.view.*

class ItemAdapter(private val items : ArrayList<Post>, private val context: Context)
    : RecyclerView.Adapter<RecyclerView.ViewHolder>(){

    class MyViewHolder(view: View) : RecyclerView.ViewHolder(view){

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return  MyViewHolder(LayoutInflater.from(context)
            .inflate(R.layout.item_post,parent,false))
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val model : Post = items[position]
        holder.itemView.tv_Id.text = model.id.toString()
        holder.itemView.tv_userId.text = model.userId
        holder.itemView.tv_name.text = model.name
        holder.itemView.tv_body.text = model.body
        holder.itemView.tv_email.text = model.email
    }

    override fun getItemCount(): Int {
        return  items.size
    }
}