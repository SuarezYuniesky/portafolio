package com.example.apipost1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.apipost1.adapter.ItemAdapter
import com.example.apipost1.api.SimpleApi
import com.example.apipost1.model.Post
import com.example.apipost1.utils.Constants
import kotlinx.android.synthetic.main.activity_main.*
import retrofit.*


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)




    btn_get_list.setOnClickListener {
     getList()
    }

    }

    private fun getRetrofit(baseURL : String): Retrofit {
        return Retrofit.Builder()
            .baseUrl(baseURL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    private fun getList(){

        val value = et_value.text.toString()
        val retrofit = getRetrofit(Constants.BASE_URL_2)

        val service : SimpleApi = retrofit
            .create<SimpleApi>(SimpleApi::class.java)

        if (value.toInt() != 0){
            Toast.makeText(this@MainActivity,value.toString(),Toast.LENGTH_LONG).show()
            val listCall : Call<List<Post>> = service.getPostList(value)

            listCall.enqueue(object : Callback<List<Post>>{
                override fun onFailure(t: Throwable?) {
                    TODO("Not yet implemented")
                }

                override fun onResponse(response: Response<List<Post>>?, retrofit: Retrofit?) {
                    if (response!!.isSuccess){
                        val post : List<Post> = response.body()
                        rv_postList.layoutManager = LinearLayoutManager(this@MainActivity)
                        rv_postList.setHasFixedSize(true)
                        val adapter = ItemAdapter(post as ArrayList<Post>,this@MainActivity)
                        rv_postList.adapter = adapter
                        //Log.i("Response","$post")
                    }
                }

            })
        }

    }
}