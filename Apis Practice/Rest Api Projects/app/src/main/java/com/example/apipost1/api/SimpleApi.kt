package com.example.apipost1.api

import com.example.apipost1.model.Post
import retrofit.Call
import retrofit.Response
import retrofit.http.GET
import retrofit.http.Path
import retrofit.http.Query

interface SimpleApi {
    @GET("comments")
    fun getPostList(
            @Query("postId") postId : String
    ) : Call<List<Post>>
}