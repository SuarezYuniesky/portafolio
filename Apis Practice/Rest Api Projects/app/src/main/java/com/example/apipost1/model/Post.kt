package com.example.apipost1.model

data class Post (
        val userId : String,
        val id : Int,
        val name : String,
        val email : String,
        val body : String
)