package com.example.newsapp.view.fragments

import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebViewClient
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import com.example.newsapp.R
import com.example.newsapp.application.ArticleEntityApplication
import com.example.newsapp.databinding.FragmentArticleDetailsBinding
import com.example.newsapp.model.entiites.Article
import com.example.newsapp.model.entiites.ArticleEntity
import com.example.newsapp.viewmodel.ArticleViewModel
import com.example.newsapp.viewmodel.ArticleViewModel.ArticleViewModelFactory


class ArticleDetailsFragment : Fragment() {


    private lateinit var mBinding : FragmentArticleDetailsBinding
    private lateinit var mArticleDetails : Article
    private lateinit var mArticlesEntityList : List<ArticleEntity>


    private val mArticleViewModel : ArticleViewModel by viewModels{
        ArticleViewModelFactory((requireActivity().application as ArticleEntityApplication).repository)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mBinding = FragmentArticleDetailsBinding.inflate(inflater,container,false)
        mBinding.fab.setOnClickListener {
         addFavArticle(mArticleDetails)
        }
        return mBinding.root
    }

    private fun addFavArticle(article: Article){
       val articleEntity = ArticleEntity(
               article.urlToImage,
               article.url,
               article.source.name,
               article.description,
               true
       )
        if (mArticlesEntityList.isNotEmpty()){
            for (item in mArticlesEntityList){
                if (item.title == articleEntity.title){
                    Toast.makeText(requireActivity(),
                            "Article Already Exist in your Favorites",
                            Toast.LENGTH_LONG).show()
                    break
                }else{
                    mArticleViewModel.insert(articleEntity)
                    mBinding.fab.setImageDrawable(
                            ContextCompat.getDrawable(
                                    requireActivity(),
                                    R.drawable.ic_favorite_selected
                            )
                    )

                    Toast.makeText(requireActivity(),
                            "Article Added to Favorites",
                            Toast.LENGTH_LONG).show()
                }
                break
            }
            /*
           mArticleViewModel.insert(articleEntity)
           mBinding.fab.setImageDrawable(
                   ContextCompat.getDrawable(
                           requireActivity(),
                           R.drawable.ic_favorite_selected
                   )
           )
           Log.e("Size ",mArticlesEntityList.size.toString())
           Toast.makeText(requireActivity(),
           "Article Added to Favorites",
           Toast.LENGTH_LONG).show()

            */
        }else{
            mArticleViewModel.insert(articleEntity)
            Toast.makeText(requireActivity(),
                    "Article Added to your Favorites",
                    Toast.LENGTH_LONG).show()
            mBinding.fab.setImageDrawable(
                    ContextCompat.getDrawable(
                            requireActivity(),
                            R.drawable.ic_favorite_selected
                    )
            )

        }

    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val args : ArticleDetailsFragmentArgs by navArgs()

        mArticleDetails = args.articleDetails

        Log.i("Article Name",args.articleDetails.title)
        displayWebView(args.articleDetails)

        mArticleViewModel.allArticlesList.observe(viewLifecycleOwner){
            articles -> articles.let {
            mArticlesEntityList = it
            Log.e("Size ",it.size.toString())
        }
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun displayWebView(article : Article){
        mBinding.wvArticlesDetails.webViewClient = WebViewClient()
        mBinding.wvArticlesDetails.apply {
            loadUrl(article.url)
        }
    }

}