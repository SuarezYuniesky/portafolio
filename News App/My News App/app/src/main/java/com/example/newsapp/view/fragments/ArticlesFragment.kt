package com.example.newsapp.view.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.observe
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.newsapp.adapters.ArticleAdapter
import com.example.newsapp.application.ArticleEntityApplication
import com.example.newsapp.databinding.FragmentArticlesBinding
import com.example.newsapp.model.entiites.Article
import com.example.newsapp.view.activities.MainActivity
import com.example.newsapp.viewmodel.ArticleViewModel
import com.example.newsapp.viewmodel.NewsViewModel

class ArticlesFragment : Fragment() {

    private lateinit var mBinding : FragmentArticlesBinding

    private lateinit var mNewsViewModel: NewsViewModel



    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
     mBinding = FragmentArticlesBinding.inflate(inflater,container,false)
        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mNewsViewModel = ViewModelProvider(this).get(NewsViewModel::class.java)

        mNewsViewModel.getNewsFromApi()

        newsViewModelObserver()
    }

    fun articleDetails(article : Article){
        findNavController().navigate(ArticlesFragmentDirections.actionNavigationHomeToArticleDetailsFragment(article))
        if (requireActivity() is MainActivity){
            (activity as MainActivity?)!!.hideBottomNavigationView()
        }
    }

    override fun onResume() {
        super.onResume()
        if (requireActivity() is MainActivity){
            (activity as MainActivity?)!!.showBottomNavigationView()
        }
    }

    private fun newsViewModelObserver(){
        mNewsViewModel.newsResponse.observe(viewLifecycleOwner) {
                newsResponse -> newsResponse?.let {
            Log.e("News","${newsResponse.articles}")
            mBinding!!.rvNewsList.layoutManager = LinearLayoutManager(requireActivity())
           val  adapter = ArticleAdapter(this,newsResponse.articles)
            mBinding!!.rvNewsList.adapter = adapter
        }
        }
        mNewsViewModel.newsLoadError.observe(viewLifecycleOwner){
                dataError -> dataError?.let {
            Log.e("News Error","$dataError")
        }
        }
        mNewsViewModel.loadNews.observe(viewLifecycleOwner){
                loadNews -> loadNews?.let {
            Log.i("News Loading","$loadNews")
        }
        }
    }

}