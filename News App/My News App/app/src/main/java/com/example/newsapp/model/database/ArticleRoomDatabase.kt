package com.example.newsapp.model.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.newsapp.model.entiites.ArticleEntity

@Database(entities = [ArticleEntity::class],version = 1)
abstract class ArticleRoomDatabase : RoomDatabase(){

    abstract fun articleDao() : ArticleEntityDao

    companion object{
        @Volatile
        private var INSTANCE : ArticleRoomDatabase? = null

        fun getDatabase(context: Context): ArticleRoomDatabase {
            // if the INSTANCE is not null, then return it,
            // if it is, then create the database
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    ArticleRoomDatabase::class.java,
                    "article_database"
                )
                    .fallbackToDestructiveMigration()
                    .build()
                INSTANCE = instance
                // return instance
                instance
            }
        }
    }
}