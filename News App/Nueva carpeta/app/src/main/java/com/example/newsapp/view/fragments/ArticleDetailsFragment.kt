package com.example.newsapp.view.fragments

import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebViewClient
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.fragment.viewModels
import androidx.navigation.fragment.navArgs
import com.example.newsapp.application.ArticleEntityApplication
import com.example.newsapp.databinding.FragmentArticleDetailsBinding
import com.example.newsapp.model.entiites.Article
import com.example.newsapp.viewmodel.ArticleViewModel


class ArticleDetailsFragment : Fragment() {


    private lateinit var mBinding : FragmentArticleDetailsBinding
    //private lateinit var mArticleDetails : Article



    private val mFavDishViewModel : ArticleViewModel by viewModels{
        ArticleViewModel.ArticleViewModelFactory((application as ArticleEntityApplication).repository)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mBinding = FragmentArticleDetailsBinding.inflate(inflater,container,false)
        mBinding.fab.setOnClickListener {

        }
        return mBinding.root
    }

    private fun addFavArticle(article: Article){

    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val args : ArticleDetailsFragmentArgs by navArgs()
        Log.i("Article Name",args.articleDetails.title)
        displayWebView(args.articleDetails)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun displayWebView(article : Article){
        mBinding.wvArticlesDetails.webViewClient = WebViewClient()
        mBinding.wvArticlesDetails.apply {
            loadUrl(article.url)
        }
    }

}