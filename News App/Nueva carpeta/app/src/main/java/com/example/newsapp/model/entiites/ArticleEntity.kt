package com.example.newsapp.model.entiites

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize


@Parcelize
@Entity(tableName = "article_table" )
data class ArticleEntity(
    @ColumnInfo val imageUrl : String,
    @ColumnInfo val sourceUrl : String,
    @ColumnInfo val title : String,
    @ColumnInfo val description : String,
    @ColumnInfo val favoriteArticle : Boolean = false,
    @PrimaryKey(autoGenerate = true) val id : Int = 0
) : Parcelable