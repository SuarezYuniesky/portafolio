package com.example.newsapp.viewmodel

import androidx.lifecycle.*
import com.example.newsapp.model.entiites.ArticleEntity
import com.example.newsapp.model.database.ArticleRepository
import kotlinx.coroutines.launch
import java.lang.IllegalArgumentException

data class ArticleViewModel(private val repository: ArticleRepository) : ViewModel(){
    fun insert(article : ArticleEntity) = viewModelScope.launch {
        repository.insertArticleData(article)
    }

    val allArticlesList : LiveData<List<ArticleEntity>> = repository.allArticlesList.asLiveData()

    class ArticleViewModelFactory(private val repository: ArticleRepository) : ViewModelProvider.Factory{
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(ArticleViewModel::class.java)){
                return ArticleViewModel(repository) as T
            }
            throw IllegalArgumentException("Unknown View Model Class")
        }

    }
}
