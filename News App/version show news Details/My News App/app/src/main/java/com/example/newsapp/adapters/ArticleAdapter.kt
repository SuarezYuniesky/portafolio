package com.example.newsapp.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.newsapp.databinding.ItemArticlesBinding
import com.example.newsapp.model.entiites.Article
import com.example.newsapp.view.fragments.ArticlesFragment

open class ArticleAdapter(private val fragment: Fragment,private val articleList : List<Article>)
    : RecyclerView.Adapter<ArticleAdapter.ViewHolder>(){

        class ViewHolder(view : ItemArticlesBinding) : RecyclerView.ViewHolder(view.root){
            val image = view.ivImageArticle
            val title = view.tvArticleTitle
            val description = view.tvDescription
            val source = view.tvSource
            val time = view.tvPublishedAt
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: ItemArticlesBinding =
            ItemArticlesBinding.inflate(LayoutInflater.from(fragment.context), parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
      val article = articleList[position]
        // Load the dish image in the ImageView.
        Glide.with(fragment)
            .load(article.urlToImage)
            .into(holder.image)
        holder.time.text = article.publishedAt
        holder.description.text = article.description
        holder.source.text = article.source.name
        holder.title.text = article.title
        holder.itemView.setOnClickListener {
            if (fragment is ArticlesFragment){
                fragment.articleDetails(article)
            }
        }
    }

    override fun getItemCount(): Int {
        return articleList.size
    }
}