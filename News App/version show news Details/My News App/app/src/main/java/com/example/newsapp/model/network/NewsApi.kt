package com.example.newsapp.model.network

import com.example.newsapp.model.entiites.News
import com.example.newsapp.utils.Constants
import io.reactivex.rxjava3.core.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface NewsApi {
@GET(Constants.END_POINT)
fun getBreakingNews(
    @Query(Constants.COUNTRY) country : String = Constants.COUNTRY_value,
    @Query(Constants.PAGE) page : Int = Constants.PAGE_VALUE,
    @Query(Constants.API_KEY) apiKey : String = Constants.API_KEY_VALUE
) : Single<News>
}