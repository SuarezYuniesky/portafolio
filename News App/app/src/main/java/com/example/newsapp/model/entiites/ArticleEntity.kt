package com.example.newsapp.model.entiites

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.versionedparcelable.ParcelField
import kotlinx.parcelize.Parcelize

@Parcelize
@Entity(tableName = "article_table" )
data class ArticleEntity(
    @ColumnInfo val image : String,
    @ColumnInfo val articleSource : String,
    @ColumnInfo val articleUrl : String,
    @ColumnInfo val title : String,
    @ColumnInfo val time : String,
    @ColumnInfo val description : String,
    @ColumnInfo val favoriteArticle : Boolean = false,
    @PrimaryKey(autoGenerate = true) val id : Int = 0
) : Parcelable