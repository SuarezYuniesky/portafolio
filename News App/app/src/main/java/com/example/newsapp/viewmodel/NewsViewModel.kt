package com.example.newsapp.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.newsapp.model.entiites.News
import com.example.newsapp.model.network.NewsApiServices
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.observers.DisposableSingleObserver
import io.reactivex.rxjava3.schedulers.Schedulers

class NewsViewModel : ViewModel(){
    private val newsApiServices = NewsApiServices()
    private val compositeDisposable = CompositeDisposable()

    val loadNews = MutableLiveData<Boolean>()
    val newsResponse = MutableLiveData<News>()
    val newsLoadError = MutableLiveData<Boolean>()
    var paginationNumber = 1
    var breakingNewsResponse : News? = null
    fun getNewsFromApi(){
        loadNews.value = true

        compositeDisposable.add(
            newsApiServices.getNews()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : DisposableSingleObserver<News>(){
                    override fun onSuccess(value: News?) {
                        paginationNumber ++
                        if (breakingNewsResponse == null){
                            breakingNewsResponse = value
                        }else{
                            val oldArticle = breakingNewsResponse!!.articles
                            val newArticle = value!!.articles
                            oldArticle.addAll(newArticle)
                            breakingNewsResponse!!.articles.addAll(oldArticle)
                        }
                        loadNews.value = false
                        newsResponse.value = breakingNewsResponse ?: value
                        newsLoadError.value = false
                    }

                    override fun onError(e: Throwable?) {
                        loadNews.value = false
                        newsLoadError.value = true
                        e!!.printStackTrace()
                    }

                })
        )
    }

    fun searchNewsFromApi(topic : String){
        loadNews.value = true

        compositeDisposable.add(
                newsApiServices.searchNews(topic)
                        .subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(object : DisposableSingleObserver<News>(){
                            override fun onSuccess(value: News?) {
                                loadNews.value = false
                                newsResponse.value = value
                                newsLoadError.value = false
                            }

                            override fun onError(e: Throwable?) {
                                loadNews.value = false
                                newsLoadError.value = true
                                e!!.printStackTrace()
                            }

                        })
        )
    }
}