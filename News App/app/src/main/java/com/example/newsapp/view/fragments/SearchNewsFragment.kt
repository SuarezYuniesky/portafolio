package com.example.newsapp.view.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.observe
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.newsapp.R
import com.example.newsapp.adapters.ArticleAdapter
import com.example.newsapp.databinding.FragmentSearchNewsBinding
import com.example.newsapp.model.entiites.Article
import com.example.newsapp.view.activities.MainActivity
import com.example.newsapp.viewmodel.NewsViewModel
import com.example.newsapp.viewmodel.NotificationsViewModel
import kotlinx.coroutines.Job
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class SearchNewsFragment : Fragment() {

    private lateinit var mNewsViewModel: NewsViewModel
    private lateinit var mBinding : FragmentSearchNewsBinding
    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        mBinding = FragmentSearchNewsBinding.inflate(inflater,container,false)
        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mNewsViewModel = ViewModelProvider(this).get(NewsViewModel::class.java)

        var job : Job? = null
        mBinding.etSearch.addTextChangedListener { editable ->
            job?.cancel()
            job = MainScope().launch {
                delay(500L)
                if (editable.toString().isNotEmpty()){
                    mNewsViewModel.searchNewsFromApi(editable.toString())
                    newsViewModelObserver()
                }
            }
        }
    }

    fun articleDetails(article : Article){
        findNavController().navigate(SearchNewsFragmentDirections.actionNavigationSearchNewsToArticleDetailsFragment(article))
        if (requireActivity() is MainActivity){
            (activity as MainActivity?)!!.hideBottomNavigationView()
        }
    }


    override fun onResume() {
        super.onResume()
        if (requireActivity() is MainActivity){
            (activity as MainActivity?)!!.showBottomNavigationView()
        }
    }
    private fun newsViewModelObserver(){
        mNewsViewModel.newsResponse.observe(viewLifecycleOwner) {
            newsResponse -> newsResponse?.let {
            Log.e("News","${newsResponse.articles.size}")

            mBinding!!.rvSearchList.layoutManager = LinearLayoutManager(requireActivity())
            val  adapter = ArticleAdapter(this,newsResponse.articles)
            mBinding!!.rvSearchList.adapter = adapter


        }
        }
        mNewsViewModel.newsLoadError.observe(viewLifecycleOwner){
            dataError -> dataError?.let {
            Log.e("News Error","$dataError")
        }
        }
        mNewsViewModel.loadNews.observe(viewLifecycleOwner){
            loadNews -> loadNews?.let {
            Log.i("News Loading","$loadNews")
        }
        }
    }
}