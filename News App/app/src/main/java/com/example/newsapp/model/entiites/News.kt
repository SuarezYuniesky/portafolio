package com.example.newsapp.model.entiites

import android.os.Parcelable

data class News(
    val articles: ArrayList<Article>,
    val status: String,
    val totalResults: Int
)