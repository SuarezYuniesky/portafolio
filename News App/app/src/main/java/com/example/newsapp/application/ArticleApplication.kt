package com.example.newsapp.application

import android.app.Application
import com.example.newsapp.model.database.ArticleRepository
import com.example.newsapp.model.database.ArticleRoomDatabase

class ArticleApplication : Application(){

    private val database by lazy {
        ArticleRoomDatabase.getDatabase(this)
    }

    val repository by lazy {
        ArticleRepository(database.articleDao())
    }
}