package com.example.newsapp.view.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.observe
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.newsapp.R
import com.example.newsapp.adapters.ArticleAdapter
import com.example.newsapp.adapters.ArticleEntityAdapter
import com.example.newsapp.application.ArticleApplication
import com.example.newsapp.databinding.FragmentFavArticlesBinding
import com.example.newsapp.model.entiites.ArticleEntity
import com.example.newsapp.view.activities.MainActivity
import com.example.newsapp.viewmodel.ArticleViewModel
import com.example.newsapp.viewmodel.ArticleViewModelFactory
import com.example.newsapp.viewmodel.DashboardViewModel

class FavArticlesFragment : Fragment() {

    private lateinit var mBinding : FragmentFavArticlesBinding

    private    val mArticleViewModel: ArticleViewModel by viewModels {
        ArticleViewModelFactory((requireActivity().application as ArticleApplication).repository)
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
       mBinding = FragmentFavArticlesBinding.inflate(inflater,container,false)
        return mBinding.root
    }

    fun articleEntityDetails(articleEntity : ArticleEntity){
        findNavController().navigate(FavArticlesFragmentDirections.actionNavigationDashboardToArticleEntityDetailsFragment(articleEntity))
        if (requireActivity() is MainActivity){
            (activity as MainActivity?)!!.hideBottomNavigationView()
        }
    }

    override fun onResume() {
        super.onResume()
        if (requireActivity() is MainActivity){
            (activity as MainActivity?)!!.showBottomNavigationView()
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mArticleViewModel.allArticlesList.observe(viewLifecycleOwner){
            articles -> articles?.let {
            mBinding.rvFavList.layoutManager = LinearLayoutManager(requireActivity())
            val adapter = ArticleEntityAdapter(this,articles)
            mBinding.rvFavList.adapter = adapter
        }
        }
    }
}