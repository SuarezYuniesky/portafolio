package com.example.newsapp.view.activities

import android.app.Dialog
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.NavigationUI
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.example.newsapp.R
import com.example.newsapp.databinding.ActivityMainBinding
import com.example.newsapp.databinding.DialogProgressBinding


class MainActivity : AppCompatActivity() {

    private lateinit var mBinding :ActivityMainBinding

    private lateinit var mNavController : NavController

    private lateinit var mProgressDialog : Dialog

    private lateinit var dialogBinding : DialogProgressBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = ActivityMainBinding.inflate(layoutInflater)
        mProgressDialog = Dialog(this)
        dialogBinding = DialogProgressBinding.inflate(layoutInflater)
        mProgressDialog.setContentView(dialogBinding.root)
        setContentView(mBinding.root)


        mNavController = findNavController(R.id.nav_host_fragment)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        val appBarConfiguration = AppBarConfiguration(setOf(
            R.id.navigation_articles,
            R.id.navigation_favorite_articles,
            R.id.navigation_search_news
        ))
        setupActionBarWithNavController(mNavController, appBarConfiguration)
        mBinding.navView.setupWithNavController(mNavController)
    }

    override fun onSupportNavigateUp(): Boolean {
        /**
         * Handles the Up button by delegating its behavior to the given NavController. This should
         * generally be called from {@link AppCompatActivity#onSupportNavigateUp()}.
         * <p>If you do not have a {@link Openable} layout, you should call
         * {@link NavController#navigateUp()} directly.
         *
         * @param navController The NavController that hosts your content.
         * @param openableLayout The Openable layout that should be opened if you are on the topmost
         *                       level of the app.
         * @return True if the {@link NavController} was able to navigate up.
         */
        return NavigationUI.navigateUp(mNavController,null)
    }

    fun hideBottomNavigationView(){
        mBinding.navView.clearAnimation()
        mBinding.navView.animate().translationY(mBinding.navView.height.toFloat()).duration = 300
    }
    fun showBottomNavigationView(){
        mBinding.navView.clearAnimation()
        mBinding.navView.animate().translationY(0f).duration = 500
    }

    fun showProgressDialog(text : String){

        dialogBinding.tvProgressText.text = text
        mProgressDialog.show()
    }

    fun hideProgressDialog(){
        mProgressDialog.dismiss()
    }


}