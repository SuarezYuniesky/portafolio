package com.example.newsapp.view.fragments

import android.app.AlertDialog
import android.os.Build
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebViewClient
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat
import androidx.core.net.toUri
import androidx.fragment.app.viewModels
import androidx.lifecycle.observe
import androidx.navigation.fragment.navArgs
import com.example.newsapp.R
import com.example.newsapp.application.ArticleApplication
import com.example.newsapp.databinding.FragmentArticleDetailsBinding
import com.example.newsapp.model.entiites.Article
import com.example.newsapp.model.entiites.ArticleEntity
import com.example.newsapp.viewmodel.ArticleViewModel
import com.example.newsapp.viewmodel.ArticleViewModelFactory


class ArticleEntityDetailsFragment : Fragment() {

    private lateinit var mBinding : FragmentArticleDetailsBinding

    private lateinit var mArticleEntityDetails : ArticleEntity

    private var deleted = false

    private    val mArticleViewModel: ArticleViewModel by viewModels {
        ArticleViewModelFactory((requireActivity().application as ArticleApplication).repository)
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        mBinding = FragmentArticleDetailsBinding.inflate(inflater,container,false)
        mBinding.fab.setOnClickListener {
          deleteFromFavorites(mArticleEntityDetails)
        }
        return mBinding.root
    }

    private fun deleteFromFavorites(articleEntity: ArticleEntity){

        if (!deleted){
            val builder = AlertDialog.Builder(requireActivity())
            builder.setTitle("Delete Article")
            builder.setMessage("Are your sure you want to delete de article?")
            builder.setIcon(android.R.drawable.ic_dialog_alert)
            builder.setPositiveButton("Yes"){ dialogInterface,_ ->

                mArticleViewModel.delete(articleEntity)

                Toast.makeText(requireActivity(),
                    "Article deleted from favorites",
                    Toast.LENGTH_LONG).show()

                mBinding.fab.setImageDrawable(
                    ContextCompat.getDrawable(
                        requireActivity(),
                        R.drawable.ic_favorite_unselected
                    )
                )
                deleted = !deleted
                
                dialogInterface.dismiss()
            }
            builder.setNegativeButton("No"){dialogInterface,_ ->
                dialogInterface.dismiss()
            }

            val alertDialog : AlertDialog = builder.create()
            alertDialog.setCancelable(false)
            alertDialog.show()
        }else{
            mArticleViewModel.insert(mArticleEntityDetails)
            mBinding.fab.setImageDrawable(
                ContextCompat.getDrawable(
                    requireActivity(),
                    R.drawable.ic_favorite_selected
                )
            )
            Toast.makeText(requireActivity(),
                "Article Added to your Favorites",
                Toast.LENGTH_LONG).show()
            deleted = false
        }


    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val args : ArticleEntityDetailsFragmentArgs by navArgs()
        mArticleEntityDetails = args.articleEntityDetails
        Log.e("Article Source",args.articleEntityDetails.articleSource)
        displayWebView(args.articleEntityDetails)

        if (args.articleEntityDetails.favoriteArticle){
            mBinding.fab.setImageDrawable(
                ContextCompat.getDrawable(
                    requireActivity(),
                    R.drawable.ic_favorite_selected
                )
            )
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun displayWebView(articleEntity : ArticleEntity){
        mBinding.wvArticlesDetails.webViewClient = WebViewClient()
        mBinding.wvArticlesDetails.apply {
            loadUrl(articleEntity.articleUrl)
        }
    }

}