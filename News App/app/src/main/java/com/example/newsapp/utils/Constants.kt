package com.example.newsapp.utils

object Constants {
    const val END_POINT : String = "v2/top-headlines"
    const val COUNTRY : String = "country"
    const val PAGE : String = "page"
    const val API_KEY : String = "apiKey"

    const val QUERY : String = "q"

    const val BASE_URL : String = "https://newsapi.org/"
    const val  API_KEY_VALUE : String = "ea4b2b83d9ca4f8092dade9df100d32a"
    const val COUNTRY_value : String = "us"
    const val PAGE_VALUE : Int = 1
}