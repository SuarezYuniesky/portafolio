package com.example.newsapp.view.fragments

import android.os.Build
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebViewClient
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat
import androidx.fragment.app.viewModels
import androidx.lifecycle.observe
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.newsapp.R
import com.example.newsapp.adapters.ArticleEntityAdapter
import com.example.newsapp.application.ArticleApplication
import com.example.newsapp.databinding.FragmentArticleDetailsBinding
import com.example.newsapp.model.entiites.Article
import com.example.newsapp.model.entiites.ArticleEntity
import com.example.newsapp.viewmodel.ArticleViewModel
import com.example.newsapp.viewmodel.ArticleViewModelFactory


class ArticleDetailsFragment : Fragment() {


    private lateinit var mBinding : FragmentArticleDetailsBinding

    private lateinit var articleDetails : Article

    private    val mArticleViewModel: ArticleViewModel by viewModels {
        ArticleViewModelFactory((requireActivity().application as ArticleApplication).repository)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mBinding = FragmentArticleDetailsBinding.inflate(inflater,container,false)
        mBinding.fab.setOnClickListener {
        addFavArticle(articleDetails)
        }
        return mBinding.root
    }

    private fun addFavArticle(article : Article){
        var exist = false
        val articleEntity = ArticleEntity(
                article.urlToImage,
                article.source.name,
                article.url,
                article.title,
                article.publishedAt,
                article.description,
                true
        )
        mArticleViewModel.allArticlesList.observe(viewLifecycleOwner){

                articles -> articles?.let {
                    if (it.isNotEmpty()){
                        for (item in it) {
                         if (item.title == articleEntity.title){
                             exist = true
                             break
                         }
                        }
                        if (exist){
                            Toast.makeText(requireActivity(),
                                    "This Article Already Exist in your Favorites",
                                    Toast.LENGTH_LONG).show()

                        }else{
                            mArticleViewModel.insert(articleEntity)
                            mBinding.fab.setImageDrawable(
                                    ContextCompat.getDrawable(
                                            requireActivity(),
                                            R.drawable.ic_favorite_selected
                                    )
                            )
                            Toast.makeText(requireActivity(),
                                    "Article Added to your Favorites",
                                    Toast.LENGTH_LONG).show()
                        }
                    }else{
                        mArticleViewModel.insert(articleEntity)
                        mBinding.fab.setImageDrawable(
                                ContextCompat.getDrawable(
                                        requireActivity(),
                                        R.drawable.ic_favorite_selected
                                )
                        )
                        Toast.makeText(requireActivity(),
                                "Article Added to your Favorites",
                                Toast.LENGTH_LONG).show()
                    }

            }

        }

    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        var exist = false
        val args : ArticleDetailsFragmentArgs by navArgs()
        articleDetails = args.articleDetails
        Log.i("Article Name",args.articleDetails.title)
        displayWebView(args.articleDetails)

        mArticleViewModel.allArticlesList.observe(viewLifecycleOwner){
                articles -> articles?.let {
             for (item in articles){
               if (item.title == articleDetails.title){
                   mBinding.fab.setImageDrawable(
                           ContextCompat.getDrawable(
                                   requireActivity(),
                                   R.drawable.ic_favorite_selected
                           )
                   )
                   break
               }

             }
        }

        }



    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun displayWebView(article : Article){
        mBinding.wvArticlesDetails.webViewClient = WebViewClient()
        mBinding.wvArticlesDetails.apply {
            loadUrl(article.url)
        }
    }

}