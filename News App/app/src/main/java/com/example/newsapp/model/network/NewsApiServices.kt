package com.example.newsapp.model.network

import com.example.newsapp.model.entiites.News
import com.example.newsapp.utils.Constants
import com.google.gson.Gson
import io.reactivex.rxjava3.core.Single
import retrofit2.Retrofit
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class NewsApiServices {
    private val api = Retrofit.Builder().baseUrl(Constants.BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
        .build()
        .create(NewsApi::class.java)

    fun getNews() : Single<News>{
        return api.getBreakingNews()
    }

    fun searchNews(topic : String) : Single<News>{
        return api.searchNews(topic)
    }
}