package com.example.newsapp.model.database

import androidx.annotation.WorkerThread
import com.example.newsapp.model.entiites.ArticleEntity
import kotlinx.coroutines.flow.Flow

class ArticleRepository(private val articleDao: ArticleDao) {

    @WorkerThread
    suspend fun insertArticleData(article: ArticleEntity){
        articleDao.insertFavoriteArticle(article)
    }

    val allArticlesList : Flow<List<ArticleEntity>> = articleDao.getAllDishesList()

    @WorkerThread
    suspend fun deleteArticleData(article: ArticleEntity){
       articleDao.deleteFavoriteArticle(article)
    }
}