package com.example.newsapp.view.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AbsListView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.observe
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.newsapp.adapters.ArticleAdapter
import com.example.newsapp.databinding.FragmentArticlesBinding
import com.example.newsapp.model.entiites.Article
import com.example.newsapp.view.activities.MainActivity
import com.example.newsapp.viewmodel.NewsViewModel

class ArticlesFragment : Fragment() {

    private lateinit var mBinding : FragmentArticlesBinding

    private lateinit var mNewsViewModel: NewsViewModel

    private var isLastPage = false
    private var isScrolling = false
    private var isLoading = false

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
     mBinding = FragmentArticlesBinding.inflate(inflater,container,false)
        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mNewsViewModel = ViewModelProvider(this).get(NewsViewModel::class.java)

        mNewsViewModel.getNewsFromApi()

        //newsViewModelObserver()
    }

    val scrollListener = object : RecyclerView.OnScrollListener(){
        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            super.onScrolled(recyclerView, dx, dy)

            val layoutManager = recyclerView.layoutManager as LinearLayoutManager
            val firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition()
            val visibleItemCount = layoutManager.childCount
            val totalItemCount = layoutManager.itemCount

            val isNotLoadingAndNotLastPage = !isLoading && !isLastPage
            val isAtLastItem = firstVisibleItemPosition + visibleItemCount >= totalItemCount
            val isNotAtBeginning = firstVisibleItemPosition >= 0
            val isTotalMoreThanVisible = totalItemCount >= 20

            val shouldPaginate = isNotLoadingAndNotLastPage && isAtLastItem
                    && isTotalMoreThanVisible && isScrolling

            if (shouldPaginate){
                newsViewModelObserver()
                isScrolling = false
            }else{
                mBinding.rvNewsList.setPadding(0,0,0,0)
            }
        }

        override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
            super.onScrollStateChanged(recyclerView, newState)
            if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL){
                isScrolling = true
            }
        }
    }

    fun articleDetails(article : Article){
        findNavController().navigate(ArticlesFragmentDirections.actionNavigationHomeToArticleDetailsFragment(article))
        if (requireActivity() is MainActivity){
            (activity as MainActivity?)!!.hideBottomNavigationView()
        }
    }

    override fun onResume() {
        super.onResume()
        if (requireActivity() is MainActivity){
            (activity as MainActivity?)!!.showBottomNavigationView()
        }
    }

    private fun newsViewModelObserver(){

        if (requireActivity() is MainActivity){
            (activity as MainActivity?)!!.showProgressDialog("Loading Items")
        }
        mNewsViewModel.newsResponse.observe(viewLifecycleOwner) {
                newsResponse -> newsResponse?.let {
            Log.e("News","${newsResponse.articles}")
            mBinding!!.rvNewsList.layoutManager = LinearLayoutManager(requireActivity())
           val  adapter = ArticleAdapter(this,newsResponse.articles)
            mBinding!!.rvNewsList.adapter = adapter

        }
        }
        mNewsViewModel.newsLoadError.observe(viewLifecycleOwner){
                dataError -> dataError?.let {
            Log.e("News Error","$dataError")
        }
        }
        mNewsViewModel.loadNews.observe(viewLifecycleOwner){
                loadNews -> loadNews?.let {
            Log.i("News Loading","$loadNews")
        }
        }
        if (requireActivity() is MainActivity){
            (activity as MainActivity?)!!.hideProgressDialog()
        }
    }

}