package com.example.newsapp.viewmodel

import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.*
import com.example.newsapp.model.database.ArticleRepository
import com.example.newsapp.model.entiites.ArticleEntity
import kotlinx.coroutines.launch
import java.lang.IllegalArgumentException

class ArticleViewModel(private val repository: ArticleRepository) : ViewModel() {

    fun insert(article : ArticleEntity) = viewModelScope.launch {
        repository.insertArticleData(article)
    }
    val allArticlesList : LiveData<List<ArticleEntity>> = repository.allArticlesList.asLiveData()


    fun delete(articleEntity : ArticleEntity) = viewModelScope.launch {
        repository.deleteArticleData(articleEntity)
    }
}

class ArticleViewModelFactory(private val repository: ArticleRepository) : ViewModelProvider.Factory{
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(ArticleViewModel::class.java)){
            return ArticleViewModel(repository) as T
        }
        throw IllegalArgumentException("Unknown View Model Class")
    }
}