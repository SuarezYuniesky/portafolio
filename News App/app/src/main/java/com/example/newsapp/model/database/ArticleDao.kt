package com.example.newsapp.model.database

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import com.example.newsapp.model.entiites.ArticleEntity
import kotlinx.coroutines.flow.Flow

@Dao
interface ArticleDao {
    @Insert
    suspend fun insertFavoriteArticle(article: ArticleEntity)

    @Query("SELECT * FROM ARTICLE_TABLE ORDER BY ID")
    fun getAllDishesList() : Flow<List<ArticleEntity>>


    @Delete
    suspend fun deleteFavoriteArticle(articleEntity : ArticleEntity)
}