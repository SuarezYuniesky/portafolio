package com.happyplaces.activities

import android.app.AlertDialog
import android.app.DatePickerDialog
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.karumi.dexter.Dexter
import kotlinx.android.synthetic.main.activity_add_happy_place.*
import java.text.SimpleDateFormat
import java.util.*
import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.ContextWrapper
import android.content.Intent
import android.database.sqlite.SQLiteDatabase
import android.graphics.Bitmap
import android.location.Location
import android.location.LocationManager
import android.net.Uri
import android.os.Looper
import android.provider.MediaStore
import android.provider.Settings
import android.util.Log
import com.google.android.gms.location.*
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.widget.Autocomplete
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode
import com.happyplaces.R
import com.happyplaces.database.DataBaseHandler
import com.happyplaces.models.HappyPlaceModel
import com.happyplaces.utils.GetAddressFromLatLng
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.io.OutputStream
import java.lang.Exception

//this class inhered frm OnClickListener class
class AddHappyPlaceActivity : AppCompatActivity() , View.OnClickListener{

    private val cal = Calendar.getInstance() //get an instance of the class calendar
    private lateinit var dateSetListener: DatePickerDialog.OnDateSetListener //date picker

    //TODO variables to save in database
    private var saveImageToInternalStorage : Uri? = null
    private var mLatitude : Double = 0.0
    private var mLongitude : Double = 0.0

    private var mHappyPlacesDetails : HappyPlaceModel? = null




    private lateinit var mFusedLocationClient : FusedLocationProviderClient
    /**
     * This function is auto created by Android when the Activity Class is created.
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        //This call the parent constructor
        super.onCreate(savedInstanceState)

        // This is used to align the xml view to this class
        setContentView(R.layout.activity_add_happy_place)

        // TODO (Step 2: Setting up the action bar using the toolbar and making enable the home back button and also adding the click of it.)
        // START
        setSupportActionBar(toolbar_add_place) // Use the toolbar to set the action bar.
        supportActionBar?.setDisplayHomeAsUpEnabled(true) // This is to use the home back button.
        // Setting the click event to the back button
        toolbar_add_place.setNavigationOnClickListener {
            onBackPressed()
        }
        // END

       mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)


      if (!Places.isInitialized()){
          Places.initialize(this@AddHappyPlaceActivity,resources.getString(R.string.google_maps_api_key))
      }

        if (intent.hasExtra(MainActivity.EXTRA_PLACE_DETAILS)){
            mHappyPlacesDetails = intent.getSerializableExtra(MainActivity.EXTRA_PLACE_DETAILS) as HappyPlaceModel
        }
        /**initialize the calendar*/

        dateSetListener = DatePickerDialog.OnDateSetListener {
                view, year, month, dayOfMonth ->
            cal.set(Calendar.YEAR,year)
            cal.set(Calendar.MONTH,month)
            cal.set(Calendar.DAY_OF_MONTH,dayOfMonth)
            updateDateInView()
        }
        /**initialize the calendar (end)*/
        updateDateInView()

        if (mHappyPlacesDetails != null){
            supportActionBar?.title = "Edit Happy Place"
            et_title.setText(mHappyPlacesDetails!!.title)
            et_description.setText(mHappyPlacesDetails!!.description)
            et_date.setText(mHappyPlacesDetails!!.date)
            et_location.setText(mHappyPlacesDetails!!.location)
            mLatitude = mHappyPlacesDetails!!.latitude
            mLongitude = mHappyPlacesDetails!!.longitude

            saveImageToInternalStorage = Uri.parse(mHappyPlacesDetails!!.image)
            iv_place_image.setImageURI(saveImageToInternalStorage)
            btn_save.text = "UPDATE"
        }
        et_date.setOnClickListener (this)
        tv_add_image.setOnClickListener(this)
        btn_save.setOnClickListener(this)
        et_location.setOnClickListener(this)
        tv_select_current_location.setOnClickListener(this)
    }

    //returns if we can get location or not
    private fun isLocationEnabled() : Boolean{
    val locationManager : LocationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
      return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)|| locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)
    }

    @SuppressLint("MissingPermission")
    private fun requestNewLocationData(){
        var mLocationRequest = LocationRequest()
        mLocationRequest.priority = LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY
        mLocationRequest.interval = 1000
        mLocationRequest.numUpdates = 1

        mFusedLocationClient.requestLocationUpdates(mLocationRequest,mLocationCallBack, Looper.myLooper())
    }

    private val mLocationCallBack = object : LocationCallback(){
        override fun onLocationResult(locationResult: LocationResult?) {
            val mLastLocation : Location = locationResult!!.lastLocation
            mLatitude = mLastLocation.latitude
            Log.i("Current Latitude","$mLatitude")
            mLongitude = mLastLocation.longitude
            Log.i("Current Longitude","$mLongitude")

            val addressTask =
                GetAddressFromLatLng(this@AddHappyPlaceActivity, mLatitude, mLongitude)

            addressTask.setAddressListener(object :
                GetAddressFromLatLng.AddressListener {
                override fun onAddressFound(address: String?) {
                    Log.e("Address ::", "" + address)
                    et_location.setText(address) // Address is set to the edittext
                }

                override fun onError() {
                    Log.e("Get Address ::", "Something is wrong...")
                }
            })

            addressTask.getAddress()
        }
    }

    /**method inhered from the class onClickListener
    to take care of all onClick events*/
    override fun onClick(v: View?) {
       when(v!!.id){
           R.id.et_date ->{

               //select a date
               DatePickerDialog(
                   this@AddHappyPlaceActivity,
                   dateSetListener,cal.get(Calendar.YEAR),
                   cal.get(Calendar.MONTH),
                   cal.get(Calendar.DAY_OF_MONTH)).show()
           }
           R.id.tv_add_image ->{
           //show alert dialog
           val pictureDialog = AlertDialog.Builder(this)
           pictureDialog.setTitle("Select Action")

           //variable to store different options of the dialog
           val pictureDialogItems = arrayOf("Select photo from Gallery",
           "Capture photo from camera")

           pictureDialog.setItems(pictureDialogItems){
               //depending of what selected i want to run some code
               dialog, which ->
               when(which){
                0 ->choosePhotoFromGallery() //method choose photo from gallery
                1 -> takePhotoFromCamera()
               }
           }
          pictureDialog.show()
       }
           R.id.btn_save ->{
               when{
                   et_title.text.isNullOrEmpty() ->{
                       Toast.makeText(this@AddHappyPlaceActivity, "Please enter title", Toast.LENGTH_SHORT).show()
                   }
                   et_description.text.isNullOrEmpty() ->{
                   Toast.makeText(this@AddHappyPlaceActivity, "Please enter description", Toast.LENGTH_SHORT).show()
               }
                   et_location.text.isNullOrEmpty() ->{
                       Toast.makeText(this@AddHappyPlaceActivity, "Please enter location", Toast.LENGTH_SHORT).show()
                   }
                   saveImageToInternalStorage == null ->{
                       Toast.makeText(this@AddHappyPlaceActivity, "Please select an image", Toast.LENGTH_SHORT).show()
                   }else ->{
                   //store code
                   val happyPlaceModel = HappyPlaceModel(
                       if (mHappyPlacesDetails == null ) 0 else mHappyPlacesDetails!!.id,
                       et_title.text.toString(),
                       saveImageToInternalStorage.toString(),
                       et_description.text.toString(),
                       et_date.text.toString(),
                       et_location.text.toString(),
                       mLongitude,mLatitude)
                   val dbHandler = DataBaseHandler(this)

                   if (mHappyPlacesDetails == null){
                       val addHappyPlace = dbHandler.addHappyPlaces(happyPlaceModel)
                       if (addHappyPlace > 0){
                           setResult(Activity.RESULT_OK)
                           finish()
                       }
                   }else{
                       val updateHappyPlace = dbHandler.updateHappyPlaces(happyPlaceModel)
                       if (updateHappyPlace > 0){
                           setResult(Activity.RESULT_OK)
                           finish()
                       }
                   }
               }
               }
           }
               R.id.et_location ->{
                   try{
                       val fields = listOf(Place.Field.ID,Place.Field.NAME,
                           Place.Field.LAT_LNG,Place.Field.ADDRESS)

                       val intent = Autocomplete.IntentBuilder(AutocompleteActivityMode.FULLSCREEN,fields)
                           .build(this@AddHappyPlaceActivity)
                       startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE)
                   }catch (e:Exception){
                       e.printStackTrace()
                   }
               }
           R.id.tv_select_current_location ->{
            if (!isLocationEnabled()){
                Toast.makeText(this,"Your location provider is turned off," +
                        "Please turn it on",Toast.LENGTH_LONG).show()
                val intent = Intent(Settings.ACTION_LOCALE_SETTINGS)
                startActivity(intent)
            }else{
                Dexter.withActivity(this)
                    .withPermissions(
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_COARSE_LOCATION
                    )
                    .withListener(object : MultiplePermissionsListener {
                        override fun onPermissionsChecked(report: MultiplePermissionsReport?) {
                            if (report!!.areAllPermissionsGranted()) {

                               requestNewLocationData()
                            }
                        }

                        override fun onPermissionRationaleShouldBeShown(
                            permissions: MutableList<PermissionRequest>?,
                            token: PermissionToken?) {
                            showRationalDialogForPermissions()
                        }
                    }).onSameThread()
                    .check()
            }
           }
       }
    }


    /**method inhered from the class onClickListener
    to take care of all onClick events (end)*/

    /**method what to do after get yes */
    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK){
            if (requestCode == GALLERY){
                if (data != null){
                    val contentURI = data.data
                    try {
                        val selectedImageBitMap = MediaStore.Images.Media.getBitmap(this.contentResolver,contentURI)
                        saveImageToInternalStorage = safeImagesToInternalStorage(selectedImageBitMap)
                        Log.e("Saved image:","Path :: $saveImageToInternalStorage ")
                        iv_place_image.setImageBitmap(selectedImageBitMap)
                    }catch (e : IOException){
                        e.printStackTrace()
                        Toast.makeText(
                            this@AddHappyPlaceActivity,
                            "Failed.",
                            Toast.LENGTH_SHORT).show()
                    }
                }
            }else if (requestCode == CAMERA){
                //convert the data we get from camera in bitmap to show it in the image view
                val thumbnail : Bitmap = data!!.extras!!.get("data") as Bitmap
                saveImageToInternalStorage  = safeImagesToInternalStorage(thumbnail)
                Log.e("Saved image:","Path :: $saveImageToInternalStorage ")
                iv_place_image.setImageBitmap(thumbnail)

            }else if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE){
                val place : Place = Autocomplete.getPlaceFromIntent(data!!)
                et_location.setText(place.address)
                mLatitude = place.latLng!!.latitude
                mLongitude = place.latLng!!.longitude
            }
    }
    }
    /**method what to do after get yes (end)*/

    /**method prepare camera to take photo*/
    private fun takePhotoFromCamera(){
        //ask for permissions
        Dexter.withContext(this)
            .withPermissions(
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.CAMERA
            ).withListener(object : MultiplePermissionsListener{
                //code to run when the permission is checked
                override fun onPermissionsChecked(report: MultiplePermissionsReport?) {
                    if (report!!.areAllPermissionsGranted()){
                        val galleryIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                        startActivityForResult(galleryIntent, CAMERA)
                    }
                }
                //code to run if the user sey no to the request
                override fun onPermissionRationaleShouldBeShown(
                    permission: MutableList<PermissionRequest>?,
                    token: PermissionToken?
                ) {
                    showRationalDialogForPermissions()
                }

            }).onSameThread().check()
    }
    /**method prepare camera to take photo (end)*/

    /**method to prepare gallery to take image*/
    private fun choosePhotoFromGallery() {
        //ask for permissions
        Dexter.withContext(this)
            .withPermissions(
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            ).withListener(object : MultiplePermissionsListener{
                //code to run when the permission is checked
                override fun onPermissionsChecked(report: MultiplePermissionsReport?) {
                    if (report!!.areAllPermissionsGranted()){
                       val galleryIntent = Intent(Intent.ACTION_PICK,
                           MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
                        startActivityForResult(galleryIntent, GALLERY)
                    }
                }
                //code to run if the user sey no to the request
                override fun onPermissionRationaleShouldBeShown(
                    permission: MutableList<PermissionRequest>?,
                    token: PermissionToken?
                ) {
                   showRationalDialogForPermissions()
                }

            }).onSameThread().check()
    }
    /**method to prepare gallery to take image (end)*/

    /**method too show rational dialog
     when the user say not to the permissions*/
    private fun showRationalDialogForPermissions() {
        AlertDialog.Builder(this).setMessage(
            "It looks like you are turned of the permission" +
                    " required for this feature. It can be enable " +
                    "under the Application Settings")

                //this code sen the user to the applications settings where can change the permissions
            .setPositiveButton("GO TO SETTINGS"){
                _,_ ->
                try{
                    val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
                    var uri = Uri.fromParts("package",packageName,null)
                    intent.data = uri
                    startActivity(intent)
                }catch (e : ActivityNotFoundException){
                    e.printStackTrace()
                }
            }.setNegativeButton("Cancel"){
                dialog, which ->  dialog.dismiss()
            }.show()
    }
    /**method too show rational dialog
    when the user say not to the permissions (end)*/

    /**format date*/
    private fun updateDateInView(){
        val myFormat = "dd.MMM.yyy"
        val sdf = SimpleDateFormat(myFormat, Locale.getDefault())
        et_date.setText(sdf.format(cal.time).toString())
    }
    /**format date (end)*/

    /**get the path of the image in the storage */
    private fun safeImagesToInternalStorage(bitmap: Bitmap) : Uri{
      val wrapper = ContextWrapper(applicationContext)
      var file = wrapper.getDir(IMAGE_DIRECTORY,Context.MODE_PRIVATE)
        file = File(file,"${UUID.randomUUID()}.jpg")

        try {
           val stream : OutputStream = FileOutputStream(file)
            bitmap.compress(Bitmap.CompressFormat.JPEG,100,stream)
            stream.flush()
            stream.close()
        }catch (e: IOException){
            e.printStackTrace()
        }
        return  Uri.parse(file.absolutePath)
    }
    /**get the path of the image in the storage (end)*/

    companion object{
        private const val GALLERY = 1
        private const val CAMERA = 2
        private const val IMAGE_DIRECTORY = "HAPPY PLACES IMAGES"
        private const val PLACE_AUTOCOMPLETE_REQUEST_CODE = 3
    }
}