package com.weatherapp.network

import com.weatherapp.models.WeatherResponse
import retrofit.Call
import retrofit.http.GET
import retrofit.http.Query

interface WeatherService {
    @GET("2.5/weather")//with this code we come to the weather section
    //with this method we come to our specific query to ask our weather
    fun getWeather(
        @Query("lat") lat : Double, //with our longitude
        @Query("lon") lon : Double,  //with our latitude
        @Query("units") units : String?,
        @Query("appid") appid : String?
    ) : Call<WeatherResponse>  //with this call we put all the data in our structure
}
