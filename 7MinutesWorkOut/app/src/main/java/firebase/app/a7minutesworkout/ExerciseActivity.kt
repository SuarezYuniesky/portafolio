package firebase.app.a7minutesworkout

import android.app.Dialog
import android.content.Intent
import android.media.MediaPlayer
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer
import android.speech.tts.TextToSpeech
import android.util.Log
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_exercise.*
import kotlinx.android.synthetic.main.dialog_custom_back_confirmation.*
import java.lang.Exception
import java.util.*
import kotlin.collections.ArrayList
import kotlin.math.log

//the class most extend of TextToSpeech to use the speaker
class ExerciseActivity : AppCompatActivity() , TextToSpeech.OnInitListener {


    private var resetTimer : CountDownTimer? = null  //variable for controlling the resting  timer
    private var restProgress = 0                    //variable for controlling the resting progress bar

    private var exerciseTimer : CountDownTimer? = null //variable for controlling the exercising  timer
    private var exerciseProgress = 0                 //variable for controlling the exercising  progress bar

    private var exerciseList : ArrayList<ExerciseModel>? = null  //variable for controlling the list of exercises
    private var currentExercisePosition = -1                     //current position of exercise

    private var tts : TextToSpeech? = null  //variable  to control the speech

    private var player : MediaPlayer? = null //viable for media player

    private var exerciseAdapter : ExercisesStatusAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_exercise)

        /**
         display toolbar--------------------------------------
         */
        setSupportActionBar(toolbar_exercise_activity)
        val actionbar = supportActionBar
        if (actionbar != null){
            actionbar.setDisplayHomeAsUpEnabled(true)
        }
        toolbar_exercise_activity.setNavigationOnClickListener {
            customDialogBackButton()
        }
        /**
        display toolbar end --------------------------------------
         */

        tts = TextToSpeech(this,this)   //set tts

        //initialize the var to tha array of exercises
        exerciseList = Constants.defaultExerciseList()

        setupRestView()
        setupExercisesStatusRecyclerView()


        }

    override fun onDestroy() {
        //destroy the related with rest timer
        if (resetTimer != null){
            resetTimer!!.cancel()
            restProgress = 0
        }
        //destroy the related with exercise  timer
        if (exerciseTimer != null){
            exerciseTimer!!.cancel()
            exerciseProgress = 0
        }
        //destroy the related with text speech
        if (tts != null){
            tts!!.stop()
            tts!!.shutdown()
        }
        //destroy the related with media player
        if (player != null){
            player!!.stop()
        }
        super.onDestroy()
    }

    /** ---------------------------------------------------------------------
    functionality for controlling resting layout
     */
    //function for controlling the progress resting bar
    private fun setRestProgressBar(){
      progressBar.progress = restProgress
      resetTimer  = object : CountDownTimer(10000,1000){
          override fun onTick(millisUntilFinished: Long) {
            restProgress ++
              progressBar.progress = 10 - restProgress
              tvTimer.text = (10 - restProgress).toString()
          }
          //what happen after time is over
          override fun onFinish() {
              currentExercisePosition ++  //increment the position of the current exercise
              exerciseList!![currentExercisePosition].setIsSelected(true)
              exerciseAdapter!!.notifyDataSetChanged()  // this line notify to the adapter all changes in current position
              setupExerciseView()

          }
      }.start()
    }
    //set up the resting view
    private fun setupRestView(){

        //working with media player
        try {
            player = MediaPlayer.create(applicationContext,R.raw.press_start)
            player!!.isLooping = false
            player!!.start()
        }catch (e : Exception){
            e.printStackTrace()
        }
        //working with media player end

        llRestView.visibility = View.VISIBLE
        llExerciseView.visibility = View.GONE
        if (resetTimer != null){
            resetTimer!!.cancel()
            restProgress = 0
        }
        tvUpcomingExerciseName.text = exerciseList!![currentExercisePosition + 1].getName()
        setRestProgressBar()
    }

    /**
    functionality for controlling resting layout  end ---------------------------------
    */


    /**-----------------------------------------------------------------------------------
   functionality for controlling exercising  layout
     */
    //function for controlling the progress exercising progress  bar
    private fun setExerciseProgressBar(){
        progressBarExercise.progress = exerciseProgress
        exerciseTimer  = object : CountDownTimer(30000,1000){
            override fun onTick(millisUntilFinished: Long) {
                exerciseProgress ++
                progressBarExercise.progress = 30 - exerciseProgress
                tvExerciseTimer.text = (30 - exerciseProgress).toString()
            }
            //what happen after time is over
            override fun onFinish() {

                if (currentExercisePosition < exerciseList?.size!! - 1){
                    exerciseList!![currentExercisePosition].setIsSelected(false)
                    exerciseList!![currentExercisePosition].setIsCompleted(true)
                    exerciseAdapter!!.notifyDataSetChanged()
                    setupRestView()
                }else{
                   finish()
                    val intent = Intent(this@ExerciseActivity,FinishActivity::class.java)
                    startActivity(intent)
                }
            }
        }.start()
    }

    //set up the exercising view
    private fun setupExerciseView(){
        llRestView.visibility = View.GONE
        llExerciseView.visibility = View.VISIBLE

        if (exerciseTimer != null){
            exerciseTimer!!.cancel()
            exerciseProgress = 0
        }
        speakOut(exerciseList!![currentExercisePosition].getName())
        setExerciseProgressBar()
        ivImage.setImageResource(exerciseList!![currentExercisePosition].getImage())
        tvExerciseName.text = exerciseList!![currentExercisePosition].getName()
    }
    /**
    functionality for controlling exercising  layout end----------------------------------
     */

    /**
    functionality for controlling text speech----------------------------------
     */
    //functionality to check if we can access textToSpeech
    override fun onInit(status: Int) {
         if (status == TextToSpeech.SUCCESS){
             val result = tts!!.setLanguage(Locale.US)
             if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_NOT_SUPPORTED)
               Log.e("TTS","The language specified is not supported!")
         }else{
             Log.e("TTS","Initialization Failed!")
         }
    }
          //functionality to speak
         private fun speakOut(text : String){
             tts!!.speak(text,TextToSpeech.QUEUE_FLUSH,null,"")
         }
    /**
    functionality for controlling text speech end ----------------------------------
     */

//this is about  recycler view
    private fun setupExercisesStatusRecyclerView(){
      rvExercisesStatus.layoutManager = LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false)
        exerciseAdapter = ExercisesStatusAdapter(exerciseList!!,this)
        rvExercisesStatus.adapter = exerciseAdapter
    }

//functionality custom dialog
    private fun customDialogBackButton(){
        val customDialog = Dialog(this)
        customDialog.setContentView(R.layout.dialog_custom_back_confirmation)
        customDialog.tvYes.setOnClickListener {
            customDialog.dismiss()
            finish()
        }
        customDialog.tvNo.setOnClickListener {
            customDialog.dismiss()
        }
        customDialog.show()
    }
    }



