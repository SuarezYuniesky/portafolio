package firebase.app.a7minutesworkout

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        llStart.setOnClickListener{
            Toast.makeText(this,"here we will start the exercise",
            Toast.LENGTH_SHORT).show()
            val intent =  Intent(this,ExerciseActivity::class.java)
            startActivity(intent)
        }

        llBMI.setOnClickListener {
            val intent =  Intent(this,BMIActivity::class.java)
            startActivity(intent)
        }
    }
}
