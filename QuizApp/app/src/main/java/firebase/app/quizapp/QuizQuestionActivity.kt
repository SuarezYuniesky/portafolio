package firebase.app.quizapp

import android.content.Intent
import android.graphics.Color
import android.graphics.Typeface
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat
import kotlinx.android.synthetic.main.activity_quiz_question.*

class QuizQuestionActivity : AppCompatActivity() ,View.OnClickListener {

    private var mCurrentPosition = 1 //contador de lista de preguntas
    private var mQuestionsList : ArrayList<Question>? = null //lista de preguntas inicializada en null
    private var mSelectedOptionPosition : Int = 0 //posicion de la pregunta seleccionada
    private var mCorrectAnswer : Int = 0
    private var mUserName : String? = null
    private var correct : Int = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_quiz_question)
        mUserName = intent.getStringExtra(Constants.USER_NAME)

        mQuestionsList = Constants.getQuestions() //instancia de la lista para poder utilizarla

        //funcion que inicilaliza todas las preguntas
        setQuestion()

        //metodos onClick de cada boton
        tv_option_one.setOnClickListener(this)
        tv_option_two.setOnClickListener(this)
        tv_option_three.setOnClickListener(this)
        tv_option_four.setOnClickListener(this)

    }

//funcion que inicilaliza todas las preguntas
       private fun setQuestion(){
    val question = mQuestionsList!![mCurrentPosition -1] // primera pregunta

    defaultFunctionsViews()  //llamado de la funcion default
    if (mCurrentPosition == mQuestionsList!!.size){ //comprovar que no hemos terinado todas las preguntas
        btn_submit.text = "FINISH"
    }else{
        btn_submit.text = "SUBMIT"
    }
           progressBar.progress = mCurrentPosition
           tv_progress.text = "$mCurrentPosition" + "/" + progressBar.max
           tv_question.text = question!!.question
           iv_image.setImageResource(question.image)
           tv_option_one.text = question.optionOne
           tv_option_two.text = question.optionTwo
           tv_option_three.text = question.optionThree
           tv_option_four.text = question.optionFour
           btn_submit.setOnClickListener(this)
       }


    //funcion para poner todos los botones en vista por defecto
          private fun defaultFunctionsViews(){
              val options = ArrayList<TextView>()
              options.add(0,tv_option_one)
              options.add(1,tv_option_two)
              options.add(2,tv_option_three)
              options.add(3,tv_option_four)

              for (option in options){
                  option.setTextColor(Color.parseColor("#7A8089"))//el mismo color de texto para todos
                  option.typeface = Typeface.DEFAULT  //apariencia por defecto del texto para todos
                  option.background = ContextCompat.getDrawable(this,R.drawable.resource)
              }
          }


//onClick metodo que llam al boton dado
    override fun onClick(v: View?) {
      when(v?.id){
          R.id.tv_option_one ->{
              selectedOptionView(tv_option_one,1)
          }
          R.id.tv_option_two ->{
              selectedOptionView(tv_option_two,2)
          }
          R.id.tv_option_three ->{
              selectedOptionView(tv_option_three,3)
          }
          R.id.tv_option_four ->{
              selectedOptionView(tv_option_four,4)
          }
          R.id.btn_submit->{
              //chequear si el usuario selecciono una opcion
              correct = 1
              if (mSelectedOptionPosition == 0){
                  mCurrentPosition++

                //chequear si la posicion actual es menor que la cantidad de pregunas
                  when {
                      mCurrentPosition <= mQuestionsList!!.size->{
                          setQuestion() //ir a la siquiente pregunta
                      }else->{
                      val intent = Intent(this,ResultActivity::class.java)
                      intent.putExtra(Constants.USER_NAME,mUserName)
                      intent.putExtra(Constants.CORRECT_ANSWER,mCorrectAnswer)
                      intent.putExtra(Constants.TOTAL_QUESTIONS,mQuestionsList!!.size)
                      startActivity(intent)
                  }
                  }
              }else{
                  //obtener la pregunta en la posicion actual
                  val question = mQuestionsList?.get(mCurrentPosition -1)
                  //chequear si es la respuesta correcta
                  if (question!!.correctAnswer != mSelectedOptionPosition){
                      answerView(mSelectedOptionPosition,R.drawable.wrong_option_border)
                  }else {
                      mCorrectAnswer ++
                  }
                  answerView(question.correctAnswer,R.drawable.correct_option_border)
                  if (mCurrentPosition == mQuestionsList!!.size){
                      btn_submit.text = "finsh"
                  }else{
                      btn_submit.text = "go to the nest question"
                  }
                  mSelectedOptionPosition = 0
              }

          }
      }
    }

    //me dice que para cada opcion seleccionada tendra un determado bachgroun
    //prepara el drawable para ser llamado
    private fun answerView(answer : Int,drawableView : Int){

        when(answer){
            1->{
                tv_option_one.background = ContextCompat.getDrawable(this,drawableView)
            }
            2->{
                tv_option_two.background = ContextCompat.getDrawable(this,drawableView)
            }
            3->{
                tv_option_three.background = ContextCompat.getDrawable(this,drawableView)
            }
            4->{
            tv_option_four.background = ContextCompat.getDrawable(this,drawableView)
        }
        }
    }


    //funcion que modifica el boton seleccionado
    //tiene como argummento un txttView y el numero de la seleccion
    private fun selectedOptionView(tv : TextView,selectedOptionNum : Int ){
        defaultFunctionsViews()
        mSelectedOptionPosition = selectedOptionNum

        tv.setTextColor(Color.parseColor("#363A43")) //color del texto
        tv.setTypeface(tv.typeface,Typeface.BOLD) //otro tipo de text
        tv.background = ContextCompat.getDrawable(this,R.drawable.selected_option_border)//cambia el background
    }
}

