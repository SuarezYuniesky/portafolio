package com.example.weatherapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.example.weatherapp.`interface`.WeatherService
import com.example.weatherapp.models.WeatherResponse
import com.example.weatherapp.utils.Constants
import retrofit.*


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        completeLink("Havana")
    }


    private fun getRetro(baseRrl : String) : Retrofit {
        return Retrofit.Builder()
                .baseUrl(baseRrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
    }

    private fun completeLink(cityName : String){
        val retrofit = getRetro(Constants.BASE_URL)

        val service : WeatherService = retrofit
                .create(WeatherService::class.java)

        val listCall : Call<WeatherResponse> =
                service.getResponse(cityName,Constants.API_KEY)


        listCall.enqueue(object : Callback<WeatherResponse>{
            override fun onFailure(t: Throwable?) {
                TODO("Not yet implemented")
            }

            override fun onResponse(response: Response<WeatherResponse>?, retrofit: Retrofit?) {
               if (response!!.isSuccess){
                   val myWeather : WeatherResponse = response.body()
                   Log.i("Response","$myWeather")
               }
            }

        })





    }
}