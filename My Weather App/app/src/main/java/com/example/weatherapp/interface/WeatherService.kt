package com.example.weatherapp.`interface`


import com.example.weatherapp.models.WeatherResponse
import retrofit.Call
import retrofit.http.GET
import retrofit.http.Query


interface WeatherService {
    @GET("2.5/weather")
     fun getResponse(
        @Query("q") q : String,
        @Query("appid") appid : String
    ) : Call<WeatherResponse>
}